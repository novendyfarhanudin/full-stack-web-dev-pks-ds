<?php

namespace App\Listeners;

use App\Mail\UserRegenOTPCode;
use App\Events\UserRegenStoredEvent;
use Illuminate\Support\Facades\Mail;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendRegenOTPCode implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegenStoredEvent  $event
     * @return void
     */
    public function handle(UserRegenStoredEvent $event)
    {
        Mail::to($event->otp_code->user->email)->send(new UserRegenOTPCode($event->otp_code));
    }
}
